require('dotenv').config();
const http = require('http');

// JSON response
const jsonResponse = (req, res) => {
  const jsonData = {
    message: 'This is a JSON response',
    success: true,
  };

  res.writeHead(200, { 'Content-Type': 'application/json' });
  res.end(JSON.stringify(jsonData));
};

// CSV response
const csvResponse = (req, res) => {
  const csvData = [
    'Name,Age,City',
    'John Doe,34,New York',
    'Jane Smith,28,Los Angeles',
  ];

  res.writeHead(200, { 'Content-Type': 'text/csv' });
  csvData.forEach((line) => {
    res.write(line + '\n');
  });
  res.end();
};

// HTML response
const htmlResponse = (req, res) => {
  const htmlData = `
    <!DOCTYPE html>
    <html lang="en">
      <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>HTML Response</title>
      </head>
      <body>
        <h1>This is an HTML response</h1>
        <p>This is a paragraph in the HTML response.</p>
      </body>
    </html>
  `;

  res.writeHead(200, { 'Content-Type': 'text/html' });
  res.end(htmlData);
};

// Create the server
const server = http.createServer((req, res) => {
  if (req.url === '/json') {
    jsonResponse(req, res);
  } else if (req.url === '/csv') {
    csvResponse(req, res);
  } else if (req.url === '/html') {
    htmlResponse(req, res);
  } else {
    res.writeHead(404);
    res.end('Not Found');
  }
});

// Start the server
const port = 3000;
server.listen(port, () => {
  console.log(`Server running at http://localhost:${port}/`);
});